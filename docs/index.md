# Welcome to "GDrive CLI Tool" Documentation 
*****

**```gdrive```** is a command line utility for interacting with Google Drive. It can be used to access and modify user data for multiple google drive accounts from any terminal.

## Configuration and Installation
******

####**Step 1: Install Go**

Before getting started, we need to install Go Language on our local machine.

<br>
####**Step 2: Create Google Credentials** 

* Go to Google API Console
* Create a new project
* Enable Google Drive API
* Configure Consent Screen
* Create credentials by choosing **OAuth client ID**

This link might help you to create your [own Google Credentials](https://github.com/mbrother2/backuptogoogle/wiki/Create-own-Google-credential-step-by-step)

<br>
####**Step 3: Download gdrive and set credentials**

After installation of Go, [download the gdrive project from github](https://github.com/gdrive-org/gdrive) and extract in a folder. Now open **handlers_drive.go** file from the root of the project directory and set your own credentials at **lines 17 & 18**.

```
const ClientId = "367116221053-7n0v**.apps.googleusercontent.com"
const ClientSecret = "1qsNodXN*****jUjmvhoO"
```

<br>
####**Step 4: Build the project**

Before building the project, we need to import **gdrive** package using this command:
```
go get github.com/prasmussen/gdrive
```
We’re about to finish. Let’s build the project. Using command line, go to the root folder of the project. Then run this command to build the project for Linux:

```
go build
```
After building the project, you’ll find a new file called **“gdrive”** on the root of the project folder. We’ll upload this file on our server.

<br>
####**Step 5: Upload and test gdrive on server**

We’ve already built GDrive with our own credentials. It’s time to test. I’m testing on **CentOS 8**. Let’s upload the generated **gdrive** file on our server.

Move GDrive to your server’s **bin** folder. The bin folder of CentOS is: ```/usr/sbin```.

```
sudo mv gdrive /usr/sbin/gdrive
```

We need to set file permission. Run this command to set permission:

```
sudo chmod 700 /server-bin-folder/gdrive
```

Run ```gdrive list``` command to start the authentication process. You will see a link like this:

```
https://accounts.google.com/o/oauth2/auth?client_id=123456789123-7n0vf****
```

Copy and paste the link in the browser. Accept the permission and you will get a verification code. It may show unverified, but just click on advanced and click ```goto <your-app-name>(unsafe)```.

Copy the verification code and paste:
```
Enter verification code: 4/9gKYAFAJ326XIP6J42t35****
```

We have successfully installed & setup GDrive and connected it to our Google Drive.

<br>
<br>

## Multiple Gdrive accounts
*****

The first time gdrive is launched (i.e. run `gdrive about` in your
terminal not just `gdrive`), you will be prompted for a verification code.
The code is obtained by following the printed url and authenticating with the
google account for the drive you want access to. This will create a token file
inside the .gdrive folder in your home directory. Note that anyone with access
to this file will also have access to your google drive.
If you want to manage multiple drives you can use the global `--config` flag
or set the environment variable `GDRIVE_CONFIG_DIR`.
Example: `GDRIVE_CONFIG_DIR="/home/user/.gdrive-secondary" gdrive list`
You will be prompted for a new verification code if the folder does not exist.

<!-- 
## Operating instructions 
******

“What is this? Where does this go?” Now is the time to demystify any assumptions around how to use your project.


## A list of files included 
******

Contingent upon how large your source code is, you may opt to not include the file tree, however you can still explain how to traverse through your code. For example, how is your code modularized? Did you use the MVC (Model, View, Controller) method? Did you use a Router system? Just a few questions to consider when detailing your file structure.


## Copyright and licensing information 
******

In order to let others know what they can and cannot do with your code, it is important to include a software license in your project. If you opt out of using a license then the default copyright laws will apply and you will retain all rights to your source code and no one may reproduce, distribute, or create derivative works from your work. Hence the reason licenses are critical and highly recommended for open source projects.


## Contact information for the distributor or ******
programmer 

Name, email, social media links, and any other helpful ways of getting in contact with you or members of your team.


## Known bugs 
******

This is the perfect place to put tickets of known issues that you are actively working on or have on backlog. Speaking of backlog, if your project is open source, this is will allow potential contributors an opportunity to review incomplete features.


## Troubleshooting 
******

In this section you will be able to highlight how your users can become troubleshooting masters for common issues encountered on your project.


## Credits and acknowledgments
******

Who were the contributing authors on the project, whose code did you reference, which tutorials did you watch that help you bring this project to fruition? Sharing is caring and all praises are due for those that have helped no matter how small the contribution.


## A changelog (usually for programmers) 
******

A changelog is a chronological list of all notable changes made to a project such as: records of changes such as bug fixes, new features, improvements, new frameworks or libraries used, and etc.


## A news section (usually for users) 
******

If your project is live and in production and you are receiving feedback from users, this is a great place to let them know, “Hey, we hear you, we appreciate you, and because of your feedback here are the most recent changes, updates, and new features made.” -->